// import {Square} from "./square.js";
// import {wallPiece} from "./wallPiece.js";
var boardSize = 9;
var matrixBoard = [];
var matrixWall = [];
var squareDimension = 60;
var wallHeight = 65;
var wallWidth = 20;

var pawnAX, pawnAY, pawnBX, pawnBY;

var moveA = 0;
var moveB = 0;
var wallA = 0;
var wallB = 0;
var robot = 0;
var computer1 = 0;
var computer2 = 0;

var wallCountA = 10
var wallCountB = 10;

var inp, inp1;
var winA;

var wallPlaced = 2;
var verticalWallPlaced = 0;
var horizontalWallPlaced = 0;
class _square {
   constructor(dimension, sRow, sColumn) {
      this.dimension = dimension;
      this.sRow = sRow;
      this.sColumn = sColumn;
   }
   position(positionX, positionY) {
      this.positionX = positionX;
      this.positionY = positionY;
   }
}
class wallPiece {
   constructor(width, height, wRow, wColumn) {
      this.width = width;
      this.hieght = height;
      this.wRow = wRow;
      this.wColumn = wColumn;
   }
   positionWall(positionXV, positionYV, positionXO, positionYO) {
      this.positionXV = positionXV;
      this.positionYV = positionYV;
      this.positionXO = positionXO;
      this.positionYO = positionYO;
   }
   visibleWall(visibleV, visibleO) {
      this.visibleV = visibleV;
      this.visibleO = visibleO;
   }
}

function setup() {
   img = loadImage('rafif--prawira-SgTLuX6t8Yo-unsplash.jpg');
   createCanvas(windowWidth - 4, windowHeight - 4);
   setBoard_Wall();
   inp = createInput('');
   inp.position(windowWidth / 4, windowHeight / (11 / 2));
   button1 = createButton('MOVE A');
   button1.position(windowWidth / 4, windowHeight / 4);
   button1.mousePressed(movePlayerA);
   button2 = createButton('PLACE WALL A');
   button2.position(windowWidth / 4, windowHeight / (9 / 2));
   button2.mousePressed(placeWallA);
   inp1 = createInput('');
   inp1.position(windowWidth / 4, windowHeight / (3 / 2));
   button3 = createButton('MOVE B');
   button3.position(windowWidth / 4, windowHeight / (4 / 3));
   button3.mousePressed(movePlayerB);
   button4 = createButton('PLACE WALL B');
   button4.position(windowWidth / 4, windowHeight / (25 / 18));
   button4.mousePressed(placeWallB);



   button5 = createButton('RESET');
   button5.position(windowWidth / 4, windowHeight / 2);
   button5.mousePressed(reset);

   button5 = createButton('Eazy Computer');
   button5.position(windowWidth / 4, windowHeight / (20 / 11));
   button5.mousePressed(vsComputer1);

   button5 = createButton('Medium Computer');
   button5.position(windowWidth / 4, windowHeight / (20 / 11) + 20);
   button5.mousePressed(vsComputer2);
}

function reset() {
   pawnAX = matrixBoard[0][4].sRow;
   pawnAY = matrixBoard[0][4].sColumn;
   pawnBX = matrixBoard[8][4].sRow;
   pawnBY = matrixBoard[8][4].sColumn;
   for (let i = 0; i < boardSize; i++) {
      for (let j = 0; j < boardSize; j++) {
         matrixWall[i][j].visibleV = false;
         matrixWall[i][j].visibleO = false;
      }
   }
   inp.value('');
   inp1.value('');
   wallCountB = 10;
   wallCountA = 10;
   computer1 = 0;
   computer2 = 0;
   winA = 0;
   button1.show();
   button2.show();
   button3.show();
   button4.show();
}

function placeWallA() {
   wallA = 1;
   if (wallCountA <= 0) {
      alert("No more walls left !");
   }
}

function placeWallB() {
   wallB = 1;
   if (wallCountB <= 0) {
      alert("No more walls left !");
   }
}

function movePlayerA() {
   moveA = 1;
}

function movePlayerB() {
   moveB = 1;
}

function vsComputer1() {
   computer1 = 1;
}

function vsComputer2() {
   computer2 = 1;
}


function draw() {
   background(img);
   drawBoard();
   pawn1();
   pawn2();
   drawWall();
   textSize(32);
   text(mouseX + " " + mouseY, 10, 30);
   fill(0, 102, 153);
   textSize(32);
   fill('green')
   text(inp.value(), windowWidth / 2, windowHeight / 7);
   fill("rgb(0, 137, 163)");
   if (computer1 == 1 || computer2 == 1) {
      text("Mister R0b0t", windowWidth / 2, windowHeight / (7 / 6));
   } else {
      text(inp1.value(), windowWidth / 2, windowHeight / (7 / 6));
   }

   fill("yellow");
   textSize(25);
   text("Walls left " + wallCountA, windowWidth / 4, windowHeight / 3);
   text("Walls left " + wallCountB, windowWidth / 4, windowHeight / (6 / 5));
   win();
   automatic();

   if (moveA == 1 || wallA == 1) {
      button1.hide();
      button2.hide();
      button3.show();
      button4.show();
   }
   if (moveB == 1 || wallB == 1 || computer1 == 1 || computer2 == 1) {
      button3.hide();
      button4.hide();
      button1.show();
      button2.show();
   }
}

function setBoard_Wall() {
   for (let i = 0; i < boardSize; i++) {
      matrixBoard[i] = new Array();
      matrixWall[i] = new Array();
   }
   for (let i = 0; i < boardSize; i++) {
      for (let j = 0; j < boardSize; j++) {
         matrixBoard[i][j] = new _square(squareDimension, i, j);
         matrixWall[i][j] = new wallPiece(wallWidth, wallHeight, i, j);
      }
   }
   y = windowHeight / 6;
   pawnAX = matrixBoard[0][4].sRow;
   pawnAY = matrixBoard[0][4].sColumn;
   pawnBX = matrixBoard[8][4].sRow;
   pawnBY = matrixBoard[8][4].sColumn;
   for (let i = 0; i < boardSize; i++) {
      x = windowWidth / (5 / 2);
      for (let j = 0; j < boardSize; j++) {
         matrixBoard[i][j].position(x, y);
         matrixWall[i][j].positionWall(x - 15, y, x, y - 15);
         matrixWall[i][j].visibleWall(false, false);
         
         x += 70;
      }
      y += 70;
   }
}

function drawBoard() {
   for (let i = 0; i < boardSize; i++) {
      for (let j = 0; j < boardSize; j++) {
         fill('rgb(98, 75, 43)');
         square(matrixBoard[i][j].positionX, matrixBoard[i][j].positionY, matrixBoard[i][j].dimension);
      }
   }
}

function drawWall() {
   for (let i = 0; i < boardSize; i++) {
      for (let j = 0; j < boardSize; j++) {
         fill("rgb(225, 155, 94)");
         if (matrixWall[i][j].visibleV == true) {
            rect(matrixWall[i][j].positionXV, matrixWall[i][j].positionYV, wallWidth, wallHeight);
         }
         if (matrixWall[i][j].visibleO == true) {
            rect(matrixWall[i][j].positionXO, matrixWall[i][j].positionYO, wallHeight, wallWidth);
         }
      }
   }
}

function pawn1() {
   fill('green');
   stroke('black');
   strokeWeight(5);
   circle(matrixBoard[pawnAX][pawnAY].positionX + 30, matrixBoard[pawnAX][pawnAY].positionY + 30, 30);
}

function pawn2() {
   fill('rgb(0, 137, 163)')
   stroke('black')
   strokeWeight(5);
   circle(matrixBoard[pawnBX][pawnBY].positionX + 30, matrixBoard[pawnBX][pawnBY].positionY + 30, 30);
}

function mousePressed() {
   if ((wallA == 1 || wallB == 1) && wallCountA > 0 && wallCountB > 0) {
      for (let i = 0; i < boardSize; i++) {
         for (let j = 0; j < boardSize; j++) {
            
            if (mouseX > matrixWall[i][j].positionXV && mouseX < matrixWall[i][j].positionXV + 20 &&
               mouseY > matrixWall[i][j].positionYV && mouseY < matrixWall[i][j].positionYV + 60 &&
               (wallPlaced == 2 || verticalWallPlaced == 1 &&
                  matrixWall[i - 1][j].visibleV == true || matrixWall[i + 1][j].visibleV == true)) {
               if ((matrixWall[i][j + 1].visibleO && matrixWall[i][j - 1].visibleO && matrixWall[i - 1][j].visibleV) ||
                  matrixWall[i + 1][j + 1].visibleO && matrixWall[i + 1][j - 1].visibleO && matrixWall[i + 1][j].visibleO) {
                  alert("You can't put walls in a cross");
               } else {
                  matrixWall[i][j].visibleV = true;
                  wallPlaced--;
                  verticalWallPlaced++;
               }

            }
            if (mouseX > matrixWall[i][j].positionXO && mouseX < matrixWall[i][j].positionXO + 60 &&
               mouseY > matrixWall[i][j].positionYO && mouseY < matrixWall[i][j].positionYO + 20 &&
               (wallPlaced == 2 || horizontalWallPlaced == 1 &&
                  matrixWall[i][j - 1].visibleO == true || matrixWall[i][j + 1].visibleO == true)) {
               if ((matrixWall[i - 1][j].visibleV && matrixWall[i + 1][j].visibleV && matrixWall[i][j - 1].visibleO) ||
                  matrixWall[i - 1][j + 1].visibleV && matrixWall[i + 1][j + 1].visibleV && matrixWall[i][j + 1].visibleO) {
                  alert("You can't put walls in a cross");
               } else {
                  matrixWall[i][j].visibleO = true;
                  wallPlaced--;
                  horizontalWallPlaced++;
               }

            }
         }
      }
      if (wallPlaced == 0) {
         if (wallA == 1) {
            wallCountA--
         } else if (wallB == 1) {
            wallCountB--
         }
         wallA = 0;
         wallB = 0;
         wallPlaced = 2;
         verticalWallPlaced = 0;
         horizontalWallPlaced = 0;
         if (computer1 == 1) {
            robot = 1;
         } else if (computer2 == 1) {
            robot = 2;
         }
      }
   }


}

function keyPressed() {
   if (keyCode === UP_ARROW) {
      if (moveA == 1) {
         if (pawnAX == 0) {
            alert("You can't go off the board");
         } else if (matrixWall[pawnAX][pawnAY].visibleO == true) {
            alert("You can't go over walls")
         } else if (pawnBX == pawnAX - 1 && pawnBY == pawnAY) {
            pawnAX -= 2;
         } else {
            pawnAX--;
            moveA = 0;
            if (computer1 == 1) {
               robot = 1;
            } else if (computer2 == 1) {
               robot = 2;
            }
         }
      }
      if (moveB == 1) {
         if (pawnBX == 0) {
            alert("Congratulations you won!")
         } else if (matrixWall[pawnBX][pawnBY].visibleO == true) {
            alert("You can't go over walls")
         } else if (pawnBX - 1 == pawnAX && pawnBY == pawnAY) {
            pawnBX -= 2;
         } else {
            pawnBX--;
            moveB = 0;
         }
      }
   }

   if (keyCode === DOWN_ARROW) {
      if (moveA == 1) {
         if (pawnAX == 8) {
            alert("Congratulations you won");
         } else if (matrixWall[pawnAX + 1][pawnAY].visibleO == true) {
            alert("You can't go over walls")
         } else if (pawnBX == pawnAX + 1 && pawnBY == pawnAY) {
            pawnAX += 2;
         } else {
            pawnAX++;
            moveA = 0;
            if (computer1 == 1) {
               robot = 1;
            } else if (computer2 == 1) {
               robot = 2;
            }
         }
      }
      if (moveB == 1) {
         if (pawnBX == 8) {
            alert("You can't go off the board")
         } else if (matrixWall[pawnBX + 1][pawnBY].visibleO == true) {
            alert("You can't go over walls")
         } else if (pawnBX == pawnAX - 1 && pawnBY == pawnAY) {
            pawnBX += 2;
         } else {
            pawnBX++;
            moveB = 0;
         }
      }
   }
   if (keyCode === LEFT_ARROW) {
      if (moveA == 1) {
         if (pawnAY == 0) {
            alert("You can't go off the board");
         } else if (matrixWall[pawnAX][pawnAY].visibleV == true) {
            alert("You can't go over walls")
         } else if (pawnBX == pawnAX && pawnBY == pawnAY - 1) {
            pawnAY -= 2;
         } else {
            pawnAY--;
            moveA = 0;
            if (computer1 == 1) {
               robot = 1;
            } else if (computer2 == 1) {
               robot = 2;
            }
         }
      }
      if (moveB == 1) {
         if (pawnBY == 0) {
            alert("You can't go off the board");
         } else if (matrixWall[pawnBX][pawnBY].visibleV == true) {
            alert("You can't go over walls")
         } else if (pawnBX == pawnAX && pawnBY == pawnAY + 1) {
            pawnBY -= 2;
         } else {
            pawnBY--;
            moveB = 0;
         }
      }
   }
   if (keyCode === RIGHT_ARROW) {
      if (moveA == 1) {
         if (pawnAY == 8) {
            alert("You can't go off the board");
         } else if (matrixWall[pawnAX][pawnAY + 1].visibleV == true) {
            alert("You can't go over walls")
         } else if (pawnAX == pawnBX && pawnAY == pawnBY - 1) {
            pawnAY += 2;
         } else {
            pawnAY++;
            moveA = 0;
            if (computer1 == 1) {
               robot = 1;
            } else if (computer2 == 1) {
               robot = 2;
            }
         }
      }
      if (moveB == 1) {
         if (pawnBY == 8) {
            alert("You can't go off the board");
         } else if (matrixWall[pawnBX][pawnBY + 1].visibleV == true) {
            alert("You can't go over walls")
         } else if (pawnBX == pawnAX && pawnBY + 1 == pawnAY) {
            pawnBY += 2;
         } else {
            pawnBY++;
            moveB = 0;
         }
      }
   }
}

function win() {
   if (winA != 0) {
      if (pawnAX == 8) {
         fill('rgb(224, 155, 247)');
         square(260, 150, 615);
         fill('pink');
         textSize(33);
         text("We have a winner -->" + inp.value(), 350, 460);
      }
      if (pawnBX == 0) {
         fill('rgb(224, 155, 247)');
         square(260, 150, 615);
         fill('pink');
         textSize(33);
         if (computer1 == 1 || computer2 == 1) {
            text("We have a winner: Mr. Robot", 350, 460);
         } else
            text("We have a winner -->" + inp1.value(), 350, 460);
      }
   }

}
var previousMove = 0;

function automatic() {
   var validMove = false;
   var validWall = false;
   var random = Math.floor(Math.random() * 2);
   if (wallCountB <= 0)
      random = 0;
   if (robot == 1) {
      if (random == 0) {
         while (!validMove) {
            var randomMove = Math.floor(Math.random() * 4);
            switch (randomMove) {
               case 0:
                  if (pawnBX != 8 && !matrixWall[pawnBX + 1][pawnBY].visibleO) {
                     if (pawnBX == pawnAX + 1 && pawnBY == pawnAY) {
                        pawnAX += 2;
                     } else {
                        pawnBX++;
                     }
                     validMove = true;
                  }
                  break;
               case 1:
                  if (pawnBY != 0 && !matrixWall[pawnBX][pawnBY].visibleV) {
                     if (pawnBX == pawnAX && pawnBY == pawnAY + 1) {
                        pawnBY -= 2;
                     } else {
                        pawnBY--;
                     }
                     validMove = true;
                  }
                  break;
               case 2:
                  if (pawnBX != 0 && !matrixWall[pawnBX][pawnBY].visibleO) {
                     if (pawnBX - 1 == pawnAX && pawnBY == pawnAY) {
                        pawnBX -= 2;
                     } else {
                        pawnBX--;
                     }
                     validMove = true;
                  }
                  break;
               case 3:
                  if (pawnBY != 8 && !matrixWall[pawnBX][pawnBY + 1].visibleV) {
                     if (pawnBX == pawnAX && pawnBY + 1 == pawnAY) {
                        pawnBY += 2;
                     } else {
                        pawnBY++;
                     }
                     validMove = true;
                  }
                  break;
            }
         }

      } else {
         while (!validWall) {
            var randomRow = Math.floor(Math.random() * 9);
            var randomColumn = Math.floor(Math.random() * 9);
            var randomOrientation = Math.floor(Math.random() * 2);
            var randomExtention = Math.floor(Math.random() * 2);

            if (randomRow != 8 && randomColumn != 8 && randomRow != 0 && randomColumn != 0 &&
               !matrixWall[randomRow][randomColumn].visibleV && !matrixWall[randomRow - 1][randomColumn].visibleV &&
               !matrixWall[randomRow + 1][randomColumn].visibleV && !matrixWall[randomRow][randomColumn].visibleO &&
               !matrixWall[randomRow][randomColumn - 1].visibleO && !matrixWall[randomRow][randomColumn + 1].visibleO) {

               if (randomOrientation == 0 && randomExtention == 0) {
                  matrixWall[randomRow][randomColumn].visibleV = true
                  matrixWall[randomRow - 1][randomColumn].visibleV = true;

               } else if (randomOrientation == 0 && randomExtention == 1) {
                  matrixWall[randomRow][randomColumn].visibleV = true;
                  matrixWall[randomRow + 1][randomColumn].visibleV = true;

               } else if (randomOrientation == 1 && randomExtention == 0) {
                  matrixWall[randomRow][randomColumn].visibleO = true;
                  matrixWall[randomRow][randomColumn - 1].visibleO = true;

               } else if (randomOrientation == 1 && randomExtention == 1) {

                  matrixWall[randomRow][randomColumn].visibleO = true;
                  matrixWall[randomRow][randomColumn + 1].visibleO = true;
               }
               wallCountB--;
               validWall = true;
            }
         }
      }
      robot = 0;
   }
   if (robot == 2) {
      var random = Math.floor(Math.random() * 2);

      var turn = 0;
      if (wallCountB <= 0) {
         random = 0;
      }
      while (!validMove) {
         if (random == 0) {
            if (!matrixWall[pawnBX][pawnBY].visibleO && previousMove != 1) {
               if (pawnBX - 1 == pawnAX && pawnBY == pawnAY) {
                  pawnBX -= 2;
               } else {
                  pawnBX--;
                  turn = 1;
               }
            } else {
               if (pawnBY == 8 && !matrixWall[pawnBX][pawnBY].visibleV) {
                  pawnBY--;
               }
               if (pawnBY == 0 && !matrixWall[pawnBX][pawnBY + 1].visibleV) {
                  pawnBY++;
               }
               if (!matrixWall[pawnBX][pawnBY + 1].visibleV && !matrixWall[pawnBX][pawnBY].visibleV && previousMove != 1) {
                  var leftSide = 0;
                  var rightSide = 0;
                  for (let row = pawnBY; row >= 0; row--) {
                     if (matrixWall[pawnBX][row].visibleO) {
                        leftSide++;
                     }
                  }
                  for (let row = pawnBY; row <= 8; row++) {
                     if (matrixWall[pawnBX][row].visibleO) {
                        rightSide++;
                     }
                  }
                  if (leftSide < rightSide) {
                     pawnBY--;
                  } else {
                     pawnBY++;
                  }
                  turn = 1;
               }

               /* if (!matrixWall[pawnBX][pawnBY].visibleV && !matrixWall[pawnBX][pawnBY - 1].visibleO) {
                  pawnBY--;
               }
               if (!matrixWall[pawnBX][pawnBY + 1].visibleV && !matrixWall[pawnBX][pawnBY + 1].visibleO) {
                  pawnBY++;
               } */

            }
            if (turn == 0 && (matrixWall[pawnBX][pawnBY].visibleV || matrixWall[pawnBX][pawnBY + 1].visibleV)) {
               previousMove = 1;
               pawnBX++;
               turn = 1;
            }
            if (previousMove == 1 && turn == 0 && (matrixWall[pawnBX][pawnBY].visibleV || matrixWall[pawnBX][pawnBY + 1].visibleV)) {
               pawnBX++;
            }
            if (turn == 0 && previousMove == 1 && matrixWall[pawnBX - 1][pawnBY].visibleV && !matrixWall[pawnBX][pawnBY].visibleV) {
               pawnBY--;
               previousMove = 0;
            }
            if (turn == 0 && previousMove == 1 && matrixWall[pawnBX - 1][pawnBY + 1].visibleV && !matrixWall[pawnBX][pawnBY + 1].visibleV) {
               pawnBY++;
               previousMove = 0;
            }
            validMove = true;
         }
         if (random == 1) {
            if (pawnAY <= 7 && pawnAX <= 6) {
               if (!matrixWall[pawnAX + 1][pawnAY].visibleO && !matrixWall[pawnAX + 1][pawnAY + 1].visibleO) {
                  matrixWall[pawnAX + 1][pawnAY].visibleO = true;
                  matrixWall[pawnAX + 1][pawnAY + 1].visibleO = true;
                  wallCountB--;
                  validMove = true;
               } else if (!matrixWall[pawnAX][pawnAY].visibleV && !matrixWall[pawnAX + 1][pawnAY].visibleV) {
                  matrixWall[pawnAX][pawnAY].visibleV = true;
                  if (!matrixWall[pawnAX + 1][pawnAY].visibleV) {
                     matrixWall[pawnAX + 1][pawnAY].visibleV = true;
                  } else
                  if (!matrixWall[pawnAX - 1][pawnAY].visibleV) {
                     matrixWall[pawnAX - 1][pawnAY].visibleV = true;
                  }
                  wallCountB--;
                  validMove = true;
               } else {
                  random = 0;
               }
            } else
               random = 0;


         }
         robot = 0;

      }
      turn = 0;
   }

}